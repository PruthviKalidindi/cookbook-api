﻿namespace CookbookApi.Model
{
    public class RecipeIngredientDetails : RecipeIngredient
    {
        public string RecipeName { get; set; }

        public string IngredientName { get; set; }

        public string MeasurementName { get; set; }
    }
}
