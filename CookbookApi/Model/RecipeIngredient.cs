﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CookbookApi.Model
{
    [Table(nameof(RecipeIngredient))]
    public class RecipeIngredient
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey(nameof(Recipe)), DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RecipeId { get; set; }

        [ForeignKey(nameof(Ingredient)), DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IngredientId { get; set; }

        public decimal Amount { get; set; }

        [ForeignKey(nameof(Measurement)), DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MeasurementId { get; set; }
    }
}
