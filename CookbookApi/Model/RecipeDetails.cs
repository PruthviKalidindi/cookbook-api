﻿using System.Collections.Generic;

namespace CookbookApi.Model
{
    public class RecipeDetails : Recipe
    {
        public List<RecipeStepDetails> RecipeSteps { get; set; }

        public List<RecipeIngredientDetails> RecipeIngredients { get; set; }
    }
}
