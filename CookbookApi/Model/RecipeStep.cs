﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CookbookApi.Model
{
    [Table(nameof(RecipeStep))]
    public class RecipeStep
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey(nameof(Recipe)), DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RecipeId { get; set; }

        public int StepNumber { get; set; }

        public string Instructions { get; set; }
    }
}
