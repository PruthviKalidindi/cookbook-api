﻿namespace CookbookApi.Model
{
    public class RecipeStepDetails : RecipeStep
    {
        public string RecipeName { get; set; }
    }
}
