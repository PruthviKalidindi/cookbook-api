﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CookbookApi.Model
{
    [Table(nameof(Recipe))]
    public class Recipe
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public int PreparationTimeInSeconds { get; set; }

        public int CookTimeInSeconds { get; set; }

        public int ReadyInSeconds { get; set; }

        public int Servings { get; set; }

        public string Image { get; set; }
    }
}
