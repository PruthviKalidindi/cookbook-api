﻿using AutoMapper;
using CookbookApi.Model;

namespace CookbookApi.Profiles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Recipe, RecipeDetails>();
            CreateMap<RecipeStep, RecipeStepDetails>();
            CreateMap<RecipeIngredient, RecipeIngredientDetails>();
        }
    }
}
