﻿using CookbookApi.Model;
using Microsoft.EntityFrameworkCore;

namespace CookbookApi.DataAccessLayer
{
    public class CookbookDbContext : DbContext
    {
        public CookbookDbContext() : base()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=Resources/Database/Cookbook.db");
        }

        public DbSet<Measurement> Measurements { get; set; }

        public DbSet<Ingredient> Ingredients { get; set; }

        public DbSet<RecipeIngredient> RecipeIngredients { get; set; }

        public DbSet<RecipeStep> RecipeSteps { get; set; }

        public DbSet<Recipe> Recipes { get; set; }
    }
}
