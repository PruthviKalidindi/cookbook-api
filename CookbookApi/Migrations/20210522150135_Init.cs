﻿using CookbookApi.Model;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CookbookApi.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: nameof(Ingredient),
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    Description = table.Column<string>(type: "TEXT", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey($"PK_{nameof(Ingredient)}", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: nameof(Ingredient),
                columns: new[] { nameof(Ingredient.Id), nameof(Ingredient.Name), nameof(Ingredient.Description) },
                values: new object[,]
                {
                    { 1, "Yogurt", "Yogurt" },
                    { 2, "Garlic Ginger Paste", "Garlic Ginger Paste" },
                    { 3, "Garam Masala", "Garam Masala" },
                    { 4, "Red Chilli Powder", "Red Chilli Powder" },
                    { 5, "Ground Turmeric", "Ground Turmeric" },
                    { 6, "Lemon Juice", "Lemon Juice" },
                    { 7, "Chicken", "Chicken" },
                    { 8, "Salt", "Salt" }
                });

            migrationBuilder.CreateTable(
                name: nameof(Measurement),
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    Description = table.Column<string>(type: "TEXT", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey($"PK_{nameof(Measurement)}", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: nameof(Measurement),
                columns: new[] { nameof(Measurement.Id), nameof(Measurement.Name), nameof(Measurement.Description) },
                values: new object[,]
                {
                    { 1, "Tablespoon", "Tablespoon" },
                    { 2, "Cup", "Cup" },
                    { 3, "Ounce", "Ounce" },
                    { 4, "Bud", "Bud" },
                    { 5, "Unit", "Unit" },
                    { 6, "KG", "Kilo Grams" }
                });

            migrationBuilder.CreateTable(
                name: nameof(Recipe),
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Title = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    Description = table.Column<string>(type: "TEXT", maxLength: 255, nullable: true),
                    PreparationTimeInSeconds = table.Column<int>(type: "INTEGER", nullable: false),
                    CookTimeInSeconds = table.Column<int>(type: "INTEGER", nullable: false),
                    ReadyInSeconds = table.Column<int>(type: "INTEGER", nullable: false),
                    Servings = table.Column<int>(type: "INTEGER", nullable: false),
                    Image = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey($"PK_{nameof(Recipe)}", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: nameof(Recipe),
                columns: new[] { nameof(Recipe.Id), nameof(Recipe.Title), nameof(Recipe.Description), nameof(Recipe.PreparationTimeInSeconds), nameof(Recipe.CookTimeInSeconds), nameof(Recipe.ReadyInSeconds), nameof(Recipe.Servings), nameof(Recipe.Image) },
                values: new object[,]
                {
                    { 1, "Chicken biryani", "Chicken biryani recipe", 900, 900, 1800, 4, "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7c/Hyderabadi_Chicken_Biryani.jpg/1920px-Hyderabadi_Chicken_Biryani.jpg" }
                });

            migrationBuilder.CreateTable(
                name: nameof(RecipeIngredient),
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    RecipeId = table.Column<int>(type: "INTEGER", nullable: false),
                    IngredientId = table.Column<int>(type: "INTEGER", nullable: false),
                    Amount = table.Column<decimal>(type: "TEXT", nullable: false),
                    MeasurementId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey($"PK_{nameof(RecipeIngredient)}", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: nameof(RecipeIngredient),
                columns: new[] { nameof(RecipeIngredient.RecipeId), nameof(RecipeIngredient.IngredientId), nameof(RecipeIngredient.Amount), nameof(RecipeIngredient.MeasurementId) },
                values: new object[,]
                {
                    { 1, 1, 3.0, 1 },
                    { 1, 2, 1.25, 1 },
                    { 1, 3, 1.0, 1 },
                    { 1, 4, 0.5, 1 },
                    { 1, 5, 0.5, 1 },
                    { 1, 6, 1.0, 1 },
                    { 1, 7, 1.0, 6 },
                    { 1, 8, 0.5, 1 }
                });

            migrationBuilder.CreateTable(
                name: nameof(RecipeStep),
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    RecipeId = table.Column<int>(type: "INTEGER", nullable: false),
                    StepNumber = table.Column<int>(type: "INTEGER", nullable: false),
                    Instructions = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey($"PK_{nameof(RecipeStep)}", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: nameof(RecipeStep),
                columns: new[] { nameof(RecipeStep.RecipeId), nameof(RecipeStep.StepNumber), nameof(RecipeStep.Instructions) },
                values: new object[,]
                {
                    { 1, 1, "Make few slits on all the chicken pieces and add ingredients to a large bowl." },
                    { 1, 2, "Mix everything well and marinate the chicken. Cover and set this aside for 1 hour. You can also rest it overnight in the fridge." },
                    { 1, 3, "Meanwhile add 2 cups basmati rice to a large pot and rinse it at least thrice. Drain and soak in fresh water for 30 mins. Drain to a colander after 30 mins." },
                    { 1, 4, "Heat ghee or oil in a heavy bottom pot or pressure cooker. Make sure you use a large one for this. Using whole spices is optional but recommended." },
                    { 1, 5, "Add thinly sliced onions. On a medium heat, fry them stirring often until uniformly light brown." },
                    { 1, 6, "This is the correct color of the onions. Do not burn them as they leave a bitter taste." },
                    { 1, 7, "Add marinated chicken & saute until it becomes pale for 5 minutes." },
                    { 1, 8, "Lower the flame completely. Cover and cook until the chicken is soft, tender and completely cooked." },
                    { 1, 9, "Check if the chicken is cooked by pricking with a fork or knife. It has to be just cooked and not overdone. Evaporate any excess moisture left in the pot by cooking further without the lid." },
                    { 1, 10, "Taste test and add more salt if needed." },
                    { 1, 11, "Mix everything well. Spread it evenly in a single layer. Level the rice gently on top. Add 2 tablespoons more mint leaves. Finally cover the pot or cooker." },
                    { 1, 12, "When the pressure releases naturally, open the lid. Gently fluff up with a fork." },
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: nameof(Ingredient));

            migrationBuilder.DropTable(
                name: nameof(Measurement));

            migrationBuilder.DropTable(
                name: nameof(Recipe));

            migrationBuilder.DropTable(
                name: nameof(RecipeIngredient));

            migrationBuilder.DropTable(
                name: nameof(RecipeStep));
        }
    }
}
