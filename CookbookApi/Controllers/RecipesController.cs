﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CookbookApi.DataAccessLayer;
using CookbookApi.Model;
using AutoMapper;

namespace CookbookApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecipesController : ControllerBase
    {
        private readonly CookbookDbContext _context;
        private readonly IMapper _mapper;

        public RecipesController(CookbookDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Recipes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Recipe>>> GetRecipes()
        {
            return await _context.Recipes.ToListAsync();
        }

        // GET: api/Recipes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Recipe>> GetRecipe(int id)
        {
            var recipe = await _context.Recipes.FindAsync(id);

            if (recipe == null)
            {
                return NotFound();
            }

            return recipe;
        }

        [HttpGet("Details/{id:int}")]
        public async Task<ActionResult<RecipeDetails>> GetRecipeDetails(int id)
        {
            var recipe = await _context.Recipes.FindAsync(id);

            if (recipe == null)
            {
                return NotFound();
            }

            var recipeSteps = _context.RecipeSteps
                .Where(s => s.RecipeId == recipe.Id)
                .OrderBy(s => s.StepNumber);

            var recipeIngredients = _context.RecipeIngredients
                .Where(i => i.RecipeId == recipe.Id);

            var recipeDetails = _mapper.Map<RecipeDetails>(recipe);
            var recipeStepDetails = recipeSteps.Select(s => _mapper.Map<RecipeStepDetails>(s));
            var recipeIngredientDetails = recipeIngredients.Select(i => _mapper.Map<RecipeIngredientDetails>(i));
            recipeDetails.RecipeSteps = await recipeStepDetails.ToListAsync();
            recipeDetails.RecipeSteps = recipeDetails.RecipeSteps.Select(s =>
            {
                s.RecipeName = recipe.Title;
                return s;
            }).ToList();
            recipeDetails.RecipeIngredients = await recipeIngredientDetails.ToListAsync();
            recipeDetails.RecipeIngredients = recipeDetails.RecipeIngredients.Select(i =>
            {
                var ingredient = _context.Ingredients.FirstOrDefault(g => g.Id == i.IngredientId);
                var measurement = _context.Measurements.FirstOrDefault(m => m.Id == i.MeasurementId);
                i.RecipeName = recipe.Title;
                i.IngredientName = ingredient != null ? ingredient.Name : string.Empty;
                i.MeasurementName = measurement != null ? measurement.Name : string.Empty;
                return i;
            }).ToList();
            return recipeDetails;
        }

        // PUT: api/Recipes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRecipe(int id, Recipe recipe)
        {
            if (id != recipe.Id)
            {
                return BadRequest();
            }

            _context.Entry(recipe).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecipeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Recipes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Recipe>> PostRecipe(Recipe recipe)
        {
            _context.Recipes.Add(recipe);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRecipe", new { id = recipe.Id }, recipe);
        }

        // DELETE: api/Recipes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Recipe>> DeleteRecipe(int id)
        {
            var recipe = await _context.Recipes.FindAsync(id);
            if (recipe == null)
            {
                return NotFound();
            }

            _context.Recipes.Remove(recipe);
            await _context.SaveChangesAsync();

            return recipe;
        }

        private bool RecipeExists(int id)
        {
            return _context.Recipes.Any(e => e.Id == id);
        }
    }
}
